# Jednoduchý Profil

Staticky generovaná stránka s profilem účastnice kurzu.

## Instalace Hugo

Nainstalujeme statický generátor Hugo. Viz https://gohugo.io/getting-started/installing/.

## Inicializace tématu

Použité téma je vloženo jako Git submodul, proto je potřeba jej nejprve inicializovat.

`git submodule update --init --recursive`

## Lokální vývoj

Lokální vývojový server spustíme pomocí příkazu:

`hugo server`

## Generování statické stránky

Statickou verzi vygenerujeme příkazem:

`hugo`

Výsledek najdeme ve složce `public`.
